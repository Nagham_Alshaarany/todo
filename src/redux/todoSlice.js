import {createSlice} from "@reduxjs/toolkit";
import moment from "moment";

const todoSlice = createSlice({
    name: 'todos',
    initialState: [
        {id: 1, title: 'Fix login && register from design', status: 'completed', date: 'january 24th, 2021 4:25 pm'},
        {id: 2, title: 'Fix login && register from design', status: 'canceled', date: 'january 24th, 2021 4:25 pm'},
        {id: 3, title: 'Fix login && register from design', status: 'active', date: 'january 24th, 2021 4:25 pm'},
    ],
    reducers: {
        addTodo: (state, action) => {
            const newTodo = {
                id: action.payload.id,
                title: action.payload.title,
                status: 'active',
                date: moment().format('MMMM Do, YYYY h:mm a'),
            };
            state.push(newTodo);
        },
        deleteTodo: (state, action) => {
            return state.filter((todo) => todo.id !== action.payload.id);
        },
        toggleCompleted: (state, action) => {
            const index = state.findIndex((todo) => todo.id === action.payload.id);
            state[index].status = action.payload.status;
        },
        toggleCanceled: (state, action) => {
            const index = state.findIndex((todo) => todo.id === action.payload.id);
            state[index].status = action.payload.status;
        },
        editTodo: (state, action) => {
            const index = state.findIndex((todo) => todo.id === action.payload.id);
            state[index].title = action.payload.title;
            state[index].date = action.payload.date;
        }
    },
});

export const {addTodo, deleteTodo, toggleCompleted, toggleCanceled, editTodo} = todoSlice.actions;
export default todoSlice.reducer;