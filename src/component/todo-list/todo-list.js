import React, {Fragment} from 'react'
import "antd/dist/antd.css";
import "./style.css"
import {List, Button, Row, Col, Descriptions} from 'antd';
import classes from 'classnames'
import {DeleteFilled} from "@ant-design/icons"
import {useDispatch, useSelector} from "react-redux";
import AddTodoModal from "../add-todo-modal";
import ActiveButton from "../active-button";
import {deleteTodo} from "../../redux/todoSlice";

const TodoList = () => {
    const data = useSelector((state => state.todos));

    const dispatch = useDispatch();
    const handleClick = (id) => {
        dispatch(deleteTodo({id: id}));
    };


    return (
        <div className={"container"}>
            <h4>Todo List</h4>
            <List
                className={'list-container'}
                footer={<AddTodoModal/>}
                bordered
                dataSource={data}
                renderItem={item => (
                    <List.Item
                        className={classes(`list-Item todo-${item.status}`)}>
                        <Row className={"row-Item"}>
                            <Col flex={"auto"}>
                                <Fragment>
                                    <h6 className={classes(`date-${item.status}`)}>{item.date}</h6>
                                    <h5 className={classes(`title-${item.status}`)}>{item.title}</h5>
                                </Fragment>

                            </Col>
                            <Col flex={0}>
                                <div style={{marginTop:5,marginRight:-5}}>
                                {item.status === 'active' ?
                                    <ActiveButton id={item.id}/> :
                                    <Button shape={'circle'} icon={<DeleteFilled className={"marked-delete-icon"}/>}
                                            onClick={()=>handleClick(item.id)}/>
                                }
                                </div>
                            </Col>
                        </Row>
                    </List.Item>
                )}
            />
        </div>
    )
};

export default TodoList;