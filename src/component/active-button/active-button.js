import React, {Fragment} from "react";
import "./style.css"
import {Button} from "antd";
import {CloseOutlined, CheckOutlined, EditFilled, DeleteFilled} from "@ant-design/icons"
import {useDispatch} from "react-redux";
import {deleteTodo, toggleCompleted, toggleCanceled} from "../../redux/todoSlice";
import AddTodoModal from "../add-todo-modal";

const ActiveButton = ({id}) => {
    const dispatch = useDispatch();
    const handleDelete = () => {
        dispatch(deleteTodo({id: id}));
    };
    const handleCompleted = () => {
        dispatch(toggleCompleted({
            id: id, status: 'completed'
        }));
    };
    const handleCanceled = () => {
        dispatch(toggleCanceled({
            id: id, status: 'canceled'
        }));
    };

    return (
        <Fragment>
            <Button shape={'circle'} icon={<DeleteFilled className={"active-icon"}/>}
                    className={"cancel-button button"} onClick={handleDelete}/>
            <AddTodoModal id={id}/>
            <Button shape={'circle'} icon={<CloseOutlined className={"active-icon"}/>}
                    className={"cancel-button button"} onClick={handleCanceled}/>
            <Button shape={'circle'} icon={<CheckOutlined className={"active-icon"}/>}
                    className={"complete-button button"} onClick={handleCompleted}/>
        </Fragment>
    )
};
export default ActiveButton;