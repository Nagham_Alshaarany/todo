import React, {useState, Fragment} from "react";
import {Modal, Button} from 'antd';
import {PlusOutlined, EditFilled} from '@ant-design/icons';
import AddTodoForm from "../add-todo-form";
import "./style.css"
import {useSelector} from "react-redux";


const AddTodoModal = ({id}) => {
    const [on, setOn] = useState(false);

    return (
        <Fragment>
            {id ? <Button onClick={() => setOn(true)} shape={'circle'} icon={<EditFilled className={"active-icon"}/>}
                          className={"edit-button"}/>
                : <Button onClick={() => setOn(true)} shape={'circle'} icon={<PlusOutlined/>}
                          className={'add-button-Item'}/>
            }
            <Modal className={"modal-Item"} visible={on} closable={false} onCancel={() => setOn(false)} footer={null}>
                <AddTodoForm onCancel={() => setOn(false)} id={id} />
            </Modal>
        </Fragment>
    )
};

export default AddTodoModal;