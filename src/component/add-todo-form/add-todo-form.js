import React, {useState} from "react";
import "antd/dist/antd.css";
import {Form, Button, Input, Row, Col} from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {Formik} from "formik";
import {addTodo, editTodo} from "../../redux/todoSlice";
import "./style.css"
import moment from "moment";

const AddTodoForm = ({onCancel, id}) => {
    const [value, setValue] = useState();
    const dispatch = useDispatch();
    const toDos = useSelector((state => state.todos));

    const handleAddSubmit = (event) => {
        dispatch(
            addTodo({
                id: toDos.length > 0 ? toDos[toDos.length - 1].id + 1 : 1,
                title: value,
            })
        );
    };
    const handleEditSubmit = (event) => {
        dispatch(editTodo({
            id: id,
            title: value,
            date: moment().format('MMMM Do, YYYY h:mm a'),
        }));
    };
    let title="";
    if(id){
        const index = toDos.findIndex((todo) => todo.id === id);
        title = toDos[index].title;
        console.log(id,title);
    }

    return (
        <div className={"add-todo-form"}>
                    <Form
                        name="addTodoForm"
                        initialValues={{NewTodo:`${id?title:''}`}}
                        onFinishFailed={() => alert('Failed to add new Todo')}
                        onFinish={id ? handleEditSubmit : handleAddSubmit}
                        layout={"inline"}
                    >
                        <Row style={{width: "100%"}}>
                            <Col flex={"auto"}>
                                <Form.Item
                                    name="NewTodo"
                                    rules={[{required: true, message: 'Please add todo title'}]}
                                    onChange={(event) => setValue(event.target.value)}
                                >
                                    <Input className={"ant-form-item-control-input-content"}
                                           placeholder={"Type Any Task..."} value={value}/>
                                </Form.Item>
                            </Col>
                            <Col flex={0}>
                                <div>
                                    <Button className={"field-button-Item"} onClick={onCancel}>
                                        Cancel
                                    </Button>
                                    <Button htmlType="submit" className={"success-button-Item"} onClick={onCancel}>
                                        Done
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
        </div>
    )
};

export default AddTodoForm;