import './App.css';
import TodoList from "./component/todo-list";
import AddTodoForm from "./component/add-todo-form";
import AddTodoModal from "./component/add-todo-modal"
import FormItemInput from "antd/es/form/FormItemInput";
function App() {
  return (
    <div className="App">
      <header className="App-header">
          <TodoList/>
      </header>
    </div>
  );
}

export default App;
